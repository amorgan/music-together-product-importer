#!/usr/bin/env python3
"""Convert filemaker exported csv to weebly product importer compatible"""

# TODO:
# Check classes in description
# Options were flipped in export, why?

import csv
import datetime

# !!! Make sure you change 'classes.csv' to the name of your .csv file !!! #
with open('classesI17.csv') as class_csv:
    ### Specific to this semester
    row_semester    = "End of Summer interim '17"           # Semester name, Example: Fall '17
    row_classes     = "3"                # Number of classes, Example: 10
    row_date        = "08/12/17"         # Class start date, Example: 09/09/17
    sem_category    = "EndofSummer17"    # Semester Category, Example: Fall Bongos 17
    class_end_date  = "09/01/2017"       # Class end date, Example: 11/14/2017
    first_child_fee = '$79'
    sibling_fee     = '$42'
    new_family_registration_fee = False  # New family registration fee? 'True' if yes, 'False' if no
    ### End specific to this semester

    class_length    = 45                   # In minutes - typically classes are 45m
    base_categories = 'Featured Products'  # Added to any generated categories
    track_inventory = 'true'
    taxable         = 'false'
    inventory       = '3'

    opt1_name = '1st Child class registration (Includes classes and semester materials)'
    opt1_type = 'dropdown'
    opt1_val  = first_child_fee

    opt2_name = 'Add a sibling over 8 months by {}? (Please make a choice)'.format(row_date)
    opt2_type = 'dropdown'
    opt2_val  = [sibling_fee, 'No thank you']

    opt3_name = 'New family registration fee? (Please make a choice)'
    opt3_type = 'dropdown'
    opt3_val  = ['$20', 'No thank you we are returning']

    opt4_name = 'Name of Child/children?'
    opt4_type = 'text (required=true, max_length=1000)'
    opt4_val  = ['', '', '', '']

    opt5_name = 'Child/children date of birth?'
    opt5_type = 'text (required=false, max_length=1000)'
    opt5_val  = ['', '', '', '']

    opt6_name = 'How did you hear about us?'
    opt6_type = 'text (required=false, max_length=1000)'
    opt6_val  = ['', '', '', '']

    classes = csv.reader(class_csv, delimiter=',')
    weebly_rows = []
    col_titles = []

    # Titles for each row, required by weebly
    col_titles.append('TITLE')
    col_titles.append('DESCRIPTION')
    col_titles.append('CATEGORIES')
    col_titles.append('TRACK INVENTORY')
    col_titles.append('TAXABLE')
    col_titles.append('PRICE')
    col_titles.append('INVENTORY')
    col_titles.append('PRODUCT TYPE')
    col_titles.append('OPTION1 NAME')
    col_titles.append('OPTION1 TYPE')
    col_titles.append('OPTION1 VALUE')
    col_titles.append('OPTION2 NAME')
    col_titles.append('OPTION2 TYPE')
    col_titles.append('OPTION2 VALUE')
    col_titles.append('OPTION3 NAME')
    col_titles.append('OPTION3 TYPE')
    col_titles.append('OPTION3 VALUE')
    col_titles.append('OPTION4 NAME')
    col_titles.append('OPTION4 TYPE')
    col_titles.append('OPTION4 VALUE')
    col_titles.append('OPTION5 NAME')
    col_titles.append('OPTION5 TYPE')
    col_titles.append('OPTION5 VALUE')

    # Check if we're doing new family registration
    if new_family_registration_fee:
        col_titles.append('OPTION6 NAME')
        col_titles.append('OPTION6 TYPE')
        col_titles.append('OPTION6 VALUE')

    weebly_rows.append(col_titles)

    for row in classes:
        # Quit if we find any empty rows
        if row[0] is '':
            break

        # Get characteristics of this class
        day = row[0]
        time_start = row[1]
        date = row[2]
        teacher_full = row[3]
        location = row[4]

        print('')
        print('Got data: {}, {}, {}, {}, {}'.format(day, time_start, date, teacher_full, location))

        # Get first name of teacher
        teacher = teacher_full.split(' ', 1)[0]
        print('Stripped first teacher\'s name: {}'.format(teacher))

        # Get 45m after above listed time
        time_format_with_date = '%Y-%m-%d %I:%M%p'
        time_format = '%I:%M%p'
        time_start_object = datetime.datetime.strptime('2010-01-01 ' + time_start, time_format_with_date) # Use dummy date, we only care about time
        time_end_object   = time_start_object + datetime.timedelta(minutes=class_length)
        class_start = time_start_object.strftime(time_format)
        class_end   = time_end_object.strftime(time_format)

        # Remove leading 0's from times (if necessary)
        if class_start.startswith('0'):
            class_start = class_start[1:]
        if class_end.startswith('0'):
            class_end = class_end[1:]
        print('Class goes from {} to {} ({}m)'.format(class_start, class_end, class_length))

        # Create title and description from above data
        title = '{} {} class {}, {}'.format(day, class_start, location, row_semester)
        print('Title: {}'.format(title))

        description = '45 minute class. %class_start%-%class_end%<br>%row_classes% classes: %date%-%class_end_date%<br>Teacher %teacher%<br>at %location%<br>'

        if new_family_registration_fee:
            description += '(If you have never taken a Music Together session before please add <a data-cke-saved-href="/store/p11/New_family_one_time_registration_fee.html" href="/store/p11/New_family_one_time_registration_fee.html">$20 new family registration fee</a>&nbsp;below.)<br>'

        description += 'Need to <strong><em>register a sibling</em></strong> who will be 8 months or older on %row_date%? &nbsp;Please add below'

        description = description.replace("%class_start%", class_start);
        description = description.replace("%class_end%", class_end);
        description = description.replace("%row_classes%", row_classes);
        description = description.replace("%date%", date);
        description = description.replace("%class_end_date%", class_end_date);
        description = description.replace("%teacher%", teacher);
        description = description.replace("%location%", location);
        description = description.replace("%row_date%", row_date);

        # Need to create 4 rows for all the different SKU options
        for index in range(4):
            product_row = []

            # Need to use special indexes for opts 1 and 2
            opt2_index = int(index % 2)
            opt3_index = int(index / 2)

            # Figure out the price of this particular product
            product_price = 0

            if opt1_val.startswith('$'):
                product_price += int(opt1_val[1:])
            if opt2_val[opt2_index].startswith('$'):
                product_price += int(opt2_val[opt2_index][1:])
            if new_family_registration_fee and opt3_val[opt3_index].startswith('$'):
                product_price += int(opt3_val[opt3_index][1:])

            product_row.append(title)
            product_row.append(description)
            product_row.append(base_categories + ', ' + sem_category)
            product_row.append(track_inventory)
            product_row.append(taxable)
            product_row.append(product_price)
            product_row.append(inventory)
            product_row.append("physical") # Product type

            product_row.append(opt1_name)
            product_row.append(opt1_type)
            product_row.append(opt1_val)                        # Always the same
            product_row.append(opt2_name)
            product_row.append(opt2_type)
            product_row.append(opt2_val[opt2_index])            # Repeats

            # Check if we're doing new family registration
            if new_family_registration_fee:
                product_row.append(opt3_name)
                product_row.append(opt3_type)
                product_row.append(opt3_val[opt3_index])        # Repeats

            product_row.append(opt4_name)
            product_row.append(opt4_type)
            product_row.append(opt4_val[index])
            product_row.append(opt5_name)
            product_row.append(opt5_type)
            product_row.append(opt5_val[index])
            product_row.append(opt6_name)
            product_row.append(opt6_type)
            product_row.append(opt6_val[index])

            # Add to global row list
            weebly_rows.append(product_row)

    # Export generated rows to csv file
    write_csv = open('import_me.csv', 'w')
    wr = csv.writer(write_csv)
    for row in weebly_rows:
        wr.writerows([row])
